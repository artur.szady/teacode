import React from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroll-component';
import { FormattedMessage } from 'react-intl';

import Loader from 'components/Loader';
import Contact from 'containers/ContactList/Contact';

import * as Styled from './styles';

const EndMessage = () => (
  <p style={{ textAlign: 'center' }}>
    <b>
      <FormattedMessage id="noMoreData" />
    </b>
  </p>
);

const ContactList = ({ users, hasMore, nextOffset }) => (
  <Styled.Wrapper>
    <InfiniteScroll
      dataLength={users.length}
      next={nextOffset}
      hasMore={hasMore}
      loader={<Loader isFetching />}
      endMessage={<EndMessage />}
    >
      {users.map((user) => (
        <Contact user={user} key={`contact-user-${user.id}`} />
      ))}
    </InfiniteScroll>
  </Styled.Wrapper>
);

ContactList.defaultProps = {
  users: [],
  hasMore: false,
};

ContactList.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
    }),
  ),
  hasMore: PropTypes.bool,
  nextOffset: PropTypes.func.isRequired,
};

export default ContactList;

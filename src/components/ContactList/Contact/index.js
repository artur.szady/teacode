import React from 'react';
import PropTypes from 'prop-types';

import * as Styled from './styles';

const Contact = ({ user, selectUser, isSelected }) => (
  <Styled.Wrapper onClick={() => selectUser(user)}>
    <Styled.DataWrapper>
      <Styled.Image src={user.avatar} />
      <Styled.Name>{`${user.first_name} ${user.last_name}`}</Styled.Name>
    </Styled.DataWrapper>
    <input type="checkbox" checked={isSelected} onChange={() => {}} />
  </Styled.Wrapper>
);

Contact.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    avatar: PropTypes.string,
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  }).isRequired,
  isSelected: PropTypes.bool.isRequired,
  selectUser: PropTypes.func.isRequired,
};

export default Contact;

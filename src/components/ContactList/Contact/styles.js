import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  &: hover {
    background: ${({ theme }) => theme.colors.cellSelect};
  }
`;

export const DataWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const Image = styled.img`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  margin: ${({ theme }) => theme.dimensions.basicMargin} 0px;
  border: 1px solid ${({ theme }) => theme.colors.border};
`;

export const Name = styled.h2`
  margin: ${({ theme }) => theme.dimensions.basicMargin};
`;

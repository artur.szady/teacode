import styled from 'styled-components';

export const Wrapper = styled.div`
  background: white;
  border-radius: ${({ theme }) => theme.dimensions.borderRadius};
  margin: ${({ theme }) => theme.dimensions.basicMargin} 0px;
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  box-shadow: ${({ theme }) => theme.colors.shadow};
`;

export default Wrapper;

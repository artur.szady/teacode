import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  margin: 64px 0px;
  justify-content: center;
  align-items: center;
  display: ${({ isFetching }) => (isFetching ? 'flex' : 'none')};
`;

export default Wrapper;

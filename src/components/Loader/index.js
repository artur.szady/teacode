import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';

import theme from 'utils/styles/theme';

import * as Styled from './styles';

const LoaderView = ({ isFetching }) => (
  <Styled.Wrapper isFetching={isFetching}>
    <Loader type="Puff" color={theme.colors.primary} height={100} width={100} />
  </Styled.Wrapper>
);

LoaderView.propTypes = {
  isFetching: PropTypes.bool.isRequired,
};

export default LoaderView;

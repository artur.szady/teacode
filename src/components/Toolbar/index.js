import React from 'react';
import { FormattedMessage } from 'react-intl';

import * as Styled from './styles';

const Toolbar = () => (
  <>
    <Styled.Wrapper>
      <FormattedMessage id="title" />
    </Styled.Wrapper>
    <Styled.FakeWrapper />
  </>
);

export default Toolbar;

import styled from 'styled-components';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  display: flex;
  align-items: center;
  color: ${({ theme }) => theme.colors.fontOposite};
  height: ${({ theme }) => theme.dimensions.toolbar};
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  background: ${({ theme }) => theme.colors.primary};
`;

export const FakeWrapper = styled.div`
  width: 100%;
  height: ${({ theme }) => theme.dimensions.toolbar};
`;

import styled from 'styled-components';

const Button = styled.button`
  outline: none;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.fontOposite};
  background: ${({ theme }) => theme.colors.primary};
  border: ${({ theme }) => theme.colors.border};
  border-radius: ${({ theme }) => theme.dimensions.borderRadius};
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  ${({ theme }) => theme.transitionAll};
  &: hover {
    background: ${({ theme }) => theme.colors.primaryDarker};
  }
`;

export default Button;

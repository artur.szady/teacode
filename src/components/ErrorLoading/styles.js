import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  text-align: center;
  padding: ${({ theme }) => theme.dimensions.basicPadding};
`;

export default Wrapper;

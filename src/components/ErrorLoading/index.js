import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Button from 'components/Button';

import * as Styled from './styles';

const ErrorLoading = ({ getUsers }) => (
  <Styled.Wrapper>
    <Button onClick={() => getUsers()}>
      <FormattedMessage id="reload" />
    </Button>
  </Styled.Wrapper>
);

ErrorLoading.propTypes = {
  getUsers: PropTypes.func.isRequired,
};

export default ErrorLoading;

import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import * as Styled from './styles';

const Search = ({ search, searchContact, intl }) => (
  <Styled.Wrapper>
    <Styled.Input
      value={search}
      placeholder={intl.formatMessage({ id: 'search' })}
      onChange={(e) => searchContact(e.target.value)}
    />
  </Styled.Wrapper>
);

Search.defaultProps = { search: '' };

Search.propTypes = {
  searchContact: PropTypes.func.isRequired,
  search: PropTypes.string,
};

export default injectIntl(Search);

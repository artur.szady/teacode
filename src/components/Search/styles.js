import styled from 'styled-components';

export const Wrapper = styled.div`
  background: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-radius: ${({ theme }) => theme.dimensions.borderRadius};
  margin: ${({ theme }) => theme.dimensions.basicMargin} 0px;
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  box-shadow: ${({ theme }) => theme.colors.shadow};
`;

export const Input = styled.input`
  width: 100%;
  margin: ${({ theme }) => theme.dimensions.basicMargin};
  outline: none;
  border: none;
  border-bottom: 1px solid ${({ theme }) => theme.colors.font};
`;

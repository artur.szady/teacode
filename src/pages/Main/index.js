import React from 'react';
import { Page } from 'utils/styles/common';
import ContactList from 'containers/ContactList';
import Search from 'containers/Search';

const Main = () => (
  <Page>
    <Search />
    <ContactList />
  </Page>
);

export default Main;

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { searchContact } from 'utils/redux/actions/main';
import { makeSelectSearch } from 'utils/redux/selectors/main';

import Search from 'components/Search';

const SearchContainer = (props) => <Search {...props} />;

const mapDispatchToProps = (dispatch) => ({
  searchContact: (value) => dispatch(searchContact(value)),
});

const mapStateToProps = createStructuredSelector({
  search: makeSelectSearch,
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default withConnect(SearchContainer);

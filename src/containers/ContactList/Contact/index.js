import React from 'react';
import { connect } from 'react-redux';

import { selectUser } from 'utils/redux/actions/main';
import { makeIsSelectUser } from 'utils/redux/selectors/main';

import Contact from 'components/ContactList/Contact';

const ContactContainer = (props) => <Contact {...props} />;

const mapDispatchToProps = (dispatch) => ({
  selectUser: (user) => dispatch(selectUser(user)),
});

const mapStateToProps = (state, ownProps) => ({
  isSelected: makeIsSelectUser(state, ownProps.user),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default withConnect(ContactContainer);

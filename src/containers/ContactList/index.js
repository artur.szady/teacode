import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  getUsers as GetUsersAction,
  nextOffset,
} from 'utils/redux/actions/main';
import {
  makeSelectUsers,
  makeSelectHasMore,
  makeSelectFetching,
  makeSelectErrorLoading,
} from 'utils/redux/selectors/main';

import ContactList from 'components/ContactList';
import ErrorLoading from 'components/ErrorLoading';
import Loader from 'components/Loader';

const ContactListContainer = (props) => {
  const { getUsers, isFetching, errorLoading } = props;
  useEffect(() => {
    getUsers();
  }, [getUsers]);
  if (isFetching) {
    return <Loader isFetching />;
  }
  if (errorLoading) {
    return <ErrorLoading {...props} />;
  }
  return <ContactList {...props} />;
};

ContactListContainer.propTypes = {
  getUsers: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  errorLoading: PropTypes.bool.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  getUsers: () => dispatch(GetUsersAction()),
  nextOffset: () => dispatch(nextOffset()),
});

const mapStateToProps = createStructuredSelector({
  users: makeSelectUsers,
  hasMore: makeSelectHasMore,
  isFetching: makeSelectFetching,
  errorLoading: makeSelectErrorLoading,
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default withConnect(ContactListContainer);

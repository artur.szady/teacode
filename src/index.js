import React from 'react';
import ReactDOM from 'react-dom';

import { ThemeProvider } from 'styled-components';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';

import configureStore from 'utils/redux/configureStore';
import { translationMessages, language } from 'utils/i18n';
import theme from 'utils/styles/theme';

import GlobalStyles from 'utils/styles/GlobalStyles';
import Toolbar from 'components/Toolbar';
import Main from 'pages/Main';

import * as serviceWorker from './serviceWorker';

const store = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <IntlProvider locale={language} messages={translationMessages[language]}>
        <ThemeProvider theme={theme}>
          <GlobalStyles />
          <Toolbar />
          <Main />
        </ThemeProvider>
      </IntlProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

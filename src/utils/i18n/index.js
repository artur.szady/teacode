import enTranslations from './translations/en.json';
import plTranslations from './translations/pl.json';

export const translationMessages = {
  en: enTranslations,
  pl: plTranslations,
};

const locales = {
  pl: 'pl',
  en: 'en',
};

const defaultLocale = 'pl';

export const language =
  locales[navigator.language.split(/[-_]/)[0]] || defaultLocale;

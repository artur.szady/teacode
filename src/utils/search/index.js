export const filterContacts = (users, search) =>
  users.filter(
    (user) =>
      user.first_name.toLowerCase().includes(search.toLowerCase()) ||
      user.last_name.toLowerCase().includes(search.toLowerCase()),
  );

export default filterContacts;

import { APP_DOMAIN } from 'utils/constants';

export const GET_USERS = `${APP_DOMAIN}/GET_USERS`;
export const SET_USERS = `${APP_DOMAIN}/SET_USERS`;
export const SELECT_USER = `${APP_DOMAIN}/SELECT_USER`;
export const SEARCH_CONTACT = `${APP_DOMAIN}/SEARCH_CONTACT`;
export const NEXT_OFFEST = `${APP_DOMAIN}/NEXT_OFFEST`;

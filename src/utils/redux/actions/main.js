import {
  GET_USERS,
  SET_USERS,
  SELECT_USER,
  SEARCH_CONTACT,
  NEXT_OFFEST,
} from '../constants';

export function getUsers() {
  return {
    type: GET_USERS,
  };
}

export function setUsers(users) {
  return {
    type: SET_USERS,
    payload: {
      users,
    },
  };
}

export function selectUser(user) {
  return {
    type: SELECT_USER,
    payload: {
      user,
    },
  };
}

export function searchContact(value) {
  return {
    type: SEARCH_CONTACT,
    payload: {
      value,
    },
  };
}

export function nextOffset() {
  return {
    type: NEXT_OFFEST,
  };
}

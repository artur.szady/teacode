import { all } from 'redux-saga/effects';
import mainSaga from './main';

export default function* root() {
  yield all([...mainSaga]);
}

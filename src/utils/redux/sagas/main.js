import { call, put, takeLatest, fork, select } from 'redux-saga/effects';

import * as APITeaCode from 'utils/api/teacode';
import * as reqAction from 'utils/api/request';
import * as RequestTypes from 'utils/api/RequestTypes';

import { setUsers } from 'utils/redux/actions/main';
import { makeSelectSelectedUsers } from 'utils/redux/selectors/main';

import { GET_USERS, SELECT_USER } from '../constants';

const fetchUsers = reqAction.fetchEntity.bind(
  null,
  reqAction.getUsers,
  APITeaCode.getUsers,
);

function* getUsers() {
  yield call(fetchUsers);
}

function* getUsersSuccess(action) {
  yield put(setUsers(action.payload));
}

function* watchGetUsers() {
  yield takeLatest(GET_USERS, getUsers);
}

function* watchGetUsersSuccess() {
  yield takeLatest(
    RequestTypes.GET_USERS[RequestTypes.SUCCESS],
    getUsersSuccess,
  );
}

function* selectUser() {
  const selectedUsers = yield select(makeSelectSelectedUsers);
  console.log(selectedUsers);
}

function* watchSelectUser() {
  yield takeLatest(SELECT_USER, selectUser);
}

export default [
  fork(watchGetUsers),
  fork(watchGetUsersSuccess),
  fork(watchSelectUser),
];

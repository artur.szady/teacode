import produce from 'immer';
import * as RequestTypes from 'utils/api/RequestTypes';
import { filterContacts } from 'utils/search';
import {
  SET_USERS,
  SELECT_USER,
  SEARCH_CONTACT,
  NEXT_OFFEST,
} from '../constants';

export const initialState = {
  users: [],
  selectedUsers: [],
  fetching: false,
  errorLoading: false,
  search: '',
  offset: 10,
  hasMore: false,
};

/* eslint-disable default-case, no-param-reassign */
const mainReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case RequestTypes.GET_USERS[RequestTypes.REQUEST]:
        draft.fetching = true;
        draft.errorLoading = false;
        break;
      case SET_USERS:
        draft.users = action.payload.users;
        draft.hasMore =
          filterContacts(draft.users, draft.search).length > draft.offset;
        break;
      case RequestTypes.GET_USERS[RequestTypes.SUCCESS]:
        draft.fetching = false;
        break;
      case RequestTypes.GET_USERS[RequestTypes.FAILURE]:
        draft.fetching = false;
        draft.errorLoading = true;
        break;
      case SELECT_USER:
        const isUserSelected = draft.selectedUsers.some(
          (user) => user.id === action.payload.user.id,
        );
        if (isUserSelected) {
          draft.selectedUsers = draft.selectedUsers.filter(
            (user) => user.id !== action.payload.user.id,
          );
        } else {
          draft.selectedUsers.push(action.payload.user);
        }
        break;
      case SEARCH_CONTACT:
        draft.search = action.payload.value;
        draft.offset = 10;
        draft.hasMore =
          filterContacts(draft.users, draft.search).length > draft.offset;
        break;
      case NEXT_OFFEST:
        draft.offset += 10;
        draft.hasMore =
          filterContacts(draft.users, draft.search).length > draft.offset;
        break;
    }
  });

export default mainReducer;

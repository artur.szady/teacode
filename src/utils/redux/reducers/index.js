import { combineReducers } from 'redux';

import mainReducer from './main';

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    main: mainReducer,
    ...injectedReducers,
  });

  return rootReducer;
}

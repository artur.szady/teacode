import { createSelector } from 'reselect';
import { filterContacts } from 'utils/search';

const selectReducer = (state) => state.main;

export const makeSelectSearch = createSelector(
  selectReducer,
  (mainState) => mainState.search,
);

export const makeSelectOffset = createSelector(
  selectReducer,
  (mainState) => mainState.offset,
);

export const makeSelectHasMore = createSelector(
  selectReducer,
  (mainState) => mainState.hasMore,
);

export const makeSelectFetching = createSelector(
  selectReducer,
  (mainState) => mainState.fetching,
);

export const makeSelectErrorLoading = createSelector(
  selectReducer,
  (mainState) => mainState.errorLoading,
);

export const makeSelectUsers = createSelector(
  [selectReducer, makeSelectSearch, makeSelectOffset],
  (mainState, search, offset) =>
    filterContacts(mainState.users, search)
      .slice()
      .sort((a, b) => {
        const textA = a.last_name.toUpperCase();
        const textB = b.last_name.toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      })
      .slice(0, offset),
);

export const makeSelectSelectedUsers = createSelector(
  selectReducer,
  (mainState) => mainState.selectedUsers,
);

const getUser = (state, user) => user;

export const makeIsSelectUser = createSelector(
  [makeSelectSelectedUsers, getUser],
  (selectedUsers, user) =>
    selectedUsers.some((selectedUser) => selectedUser.id === user.id),
);

import { put, call } from 'redux-saga/effects';

import * as RequestTypes from './RequestTypes';
import { createRequestAction } from './action';

export function* fetchEntity(entity, apiFn, payload, ...otherParams) {
  if (entity.request) {
    yield put(entity.request(payload));
  }

  try {
    const response = yield call(apiFn, payload, ...otherParams);
    if (entity.success) {
      yield put(entity.success(response));
    }
    return response;
  } catch (error) {
    if (entity.failure) {
      yield put(entity.failure(error));
    }
    return null;
  }
}

export const getUsers = createRequestAction(RequestTypes.GET_USERS);

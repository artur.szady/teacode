import * as RequestTypes from './RequestTypes';

const action = (type, payload = {}) => {
  const rs = { payload, type };
  return rs;
};

export const createRequestAction = (requestType) => ({
  request: (payload) => action(requestType[RequestTypes.REQUEST], payload),
  success: (response) => action(requestType[RequestTypes.SUCCESS], response),
  failure: (error) => action(requestType[RequestTypes.FAILURE], error),
});

export default createRequestAction;

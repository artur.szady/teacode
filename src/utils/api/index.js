import axios from 'axios';

class API {
  constructor(baseURL, config) {
    this.config = config;
    this.axios = axios.create({
      baseURL,
      timeout: 5000,
      ...this.config,
    });
  }

  get(endpoint, params) {
    return this.axios
      .get(endpoint, { ...this.config, params })
      .then((rs) => rs.data);
  }

  post(endpoint, params) {
    return this.axios
      .post(endpoint, { ...this.config, params })
      .then((rs) => rs.data);
  }

  put(endpoint, params) {
    return this.axios
      .put(endpoint, { ...this.config, params })
      .then((rs) => rs.data);
  }
}

export default API;

import API from '.';

export const BASE_URL = process.env.REACT_APP_BASE_URL;

export const APITeaCode = new API(BASE_URL, {
  header: {
    'Content-Type': 'application/json',
  },
});

export const getUsers = () => APITeaCode.get('/users.json');

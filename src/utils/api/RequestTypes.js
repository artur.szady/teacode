import { APP_DOMAIN } from 'utils/constants';

export const REQUEST = 'REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';

function createRequestTypes(base) {
  return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
    acc[type] = `${APP_DOMAIN}/${base}_${type}`;
    return acc;
  }, {});
}

export const GET_USERS = createRequestTypes('GET_USERS');

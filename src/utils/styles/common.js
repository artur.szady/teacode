import styled from 'styled-components';

export const Page = styled.div`
  margin: auto;
  padding: ${({ theme }) => theme.dimensions.basicPadding};
  margin-top: ${({ theme }) => theme.dimensions.basicMargin};
  max-width: ${({ theme }) => theme.dimensions.maxWidth};
`;

export default Page;

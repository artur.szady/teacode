import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    font-family: Open Sans;
    background: ${({ theme }) => theme.colors.background};
    color: ${({ theme }) => theme.colors.font};
    font-size: ${({ theme }) => theme.dimensions.font};
    margin: 0px;
    @media (max-width: ${({ theme }) => theme.dimensions.mobileBreakpoint}) {
      font-size: ${({ theme }) => theme.dimensions.fontMobile};
    }
  }

  input {
    color: ${({ theme }) => theme.colors.font};
    font-size: ${({ theme }) => theme.dimensions.font};
    @media (max-width: ${({ theme }) => theme.dimensions.mobileBreakpoint}) {
      font-size: ${({ theme }) => theme.dimensions.fontMobile};
    }
  }
`;

export default GlobalStyle;

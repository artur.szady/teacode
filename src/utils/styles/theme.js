const theme = {
  colors: {
    primary: '#01adff',
    primaryDarker: '#0d88c1',
    background: '#f4f8f9',
    font: '#434343',
    fontOposite: 'white',
    border: '#434343',
    shadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
    cellSelect: '#D8D8D8',
  },
  dimensions: {
    basicMargin: '8px',
    basicPadding: '8px',
    maxWidth: '1100px',
    mobileBreakpoint: '450px',
    borderRadius: '4px',
    toolbar: '32px',
    font: '20px',
    fontMobile: '12px',
  },
  transitionAll: 'transition: all 0.3s ease',
};

export default theme;
